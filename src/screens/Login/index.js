import React from 'react';
import { SafeAreaView, View, Text, TextInput, TouchableOpacity } from 'react-native';
import { style } from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Auth from '@react-native-firebase/auth';

class Login extends React.Component {

    constructor() {
        super();

        this.state = {
            email: '',
            password: ''
        }
    }

    login(email, password) {
        if (email != '' && password != '') {
            Auth().signInWithEmailAndPassword(email, password).then(e => {
                alert(JSON.stringify(e));
            }).catch(e => {
                alert(e);
            })
        } else {
            alert('Please Enter your data')
        }
    }

    render() {
        const { container, backIcon, InputContainer, head, btn, btnTitle } = style;
        return (
            <SafeAreaView style={container}>
                <Icon name="chevron-left" style={backIcon} onPress={() => {
                    if (this.props.navigation.canGoBack()) {
                        this.props.navigation.goBack();
                    }
                }} />

                <Text style={head}>Log In</Text>

                <View style={InputContainer}>
                    <TextInput
                        placeholder="E-mail"
                        placeholderTextColor="#aaa"
                        onChangeText={email => this.setState({ email })}
                        value={this.state.email}
                    />
                </View>
                <View style={InputContainer}>
                    <TextInput
                        placeholder="Password"
                        placeholderTextColor="#aaa"
                        onChangeText={password => this.setState({ password })}
                        secureTextEntry
                        value={this.state.password}
                    />
                </View>

                <TouchableOpacity style={btn} onPress={() => this.login(this.state.email, this.state.password)}>
                    <Text style={btnTitle}>Log in</Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

export default Login;