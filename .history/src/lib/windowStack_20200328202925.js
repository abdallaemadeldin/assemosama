import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from './../screens/Splash';
import Login from './../screens/Login';

const Stack = createStackNavigator();

class WindowStack extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator headerMode="none" initialRouteName="Splash">
                    <Stack.Screen name="Splash" component={Splash} />
                    <Stack.Screen name="Login" component={Login} />
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

export default WindowStack;