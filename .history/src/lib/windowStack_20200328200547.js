import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

class WindowStack extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator headerMode="none" initialRouteName="Splash">

                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}