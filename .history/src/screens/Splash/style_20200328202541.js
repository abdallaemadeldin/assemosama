import { StyleSheet, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignItems: 'center',
        paddingTop: '20%'
    },
    logo: {
        width: 150,
        height: 150,
        tintColor: "#F95866"
    },
    head: {
        fontSize: 30,
        color: "#F95866",
        fontWeight: '700',
        marginTop: '10%'
    },
    info: {
        fontSize: 18,
        color: '#000',
        fontWeight: '600',
        marginVertical: '5%'
    },
    btn: {
        width: '75%',
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#F95866",
        borderRadius: 22,
        marginTop: height * 1 / 100
    },
    btnTitle: {
        fontSize: 16,
        color: '#fff'
    }
});