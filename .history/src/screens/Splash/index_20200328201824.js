import React from 'react';
import { SafeAreaView, Image, Text } from 'react-native';
import { style } from './style';

class Splash extends React.Component {
    render() {
        const { logo, container } = style;
        return (
            <SafeAreaView style={container}>
                <Image source={require('./../../assets/logo.png')} style={logo} />

                <Text></Text>
            </SafeAreaView>
        );
    }
}

export default Splash;