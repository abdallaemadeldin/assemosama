import React from 'react';
import { SafeAreaView, Image } from 'react-native';
import { style } from './style';

class Splash extends React.Component {
    render() {
        const { logo, container } = style;
        return (
            <SafeAreaView style={container}>
                <Image source={require('./../../assets/logo.png')} style={logo} />
            </SafeAreaView>
        );
    }
}

export default Splash;