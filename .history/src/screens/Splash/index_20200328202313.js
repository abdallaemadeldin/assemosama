import React from 'react';
import { SafeAreaView, Image, Text, TouchableOpacity } from 'react-native';
import { style } from './style';

class Splash extends React.Component {
    render() {
        const { logo, container, head, info, btn, btnTitle } = style;
        return (
            <SafeAreaView style={container}>
                <Image source={require('./../../assets/logo.png')} style={logo} />

                <Text style={head}>Welcome to your app</Text>
                <Text style={info}>{'Start your IOS app with this\nFirebase Swift Starter Kit.'}</Text>

                <TouchableOpacity style={btn}>
                    <Text style={btnTitle}>Log in</Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

export default Splash;