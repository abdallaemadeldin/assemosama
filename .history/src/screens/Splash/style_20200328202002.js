import { StyleSheet, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 150,
        height: 150,
        tintColor: "#F95866"
    },
    head: {
        fontSize: 28,
        color: "#F95866",
        fontWeight: '700'
    }
});