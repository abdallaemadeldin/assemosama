import { StyleSheet, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignItems: 'center',
        paddingTop: '15%'
    },
    logo: {
        width: 150,
        height: 150,
        tintColor: "#F95866"
    },
    head: {
        fontSize: 30,
        color: "#F95866",
        fontWeight: '700'
    }
});