import React from 'react';
import { SafeAreaView, Image } from 'react-native';
import { style } from './style';

class Splash extends React.Component {
    render() {
        return (
            <SafeAreaView>
                <Image source={require('./../../assets/logo.png')} />
            </SafeAreaView>
        );
    }
}

export default Splash;