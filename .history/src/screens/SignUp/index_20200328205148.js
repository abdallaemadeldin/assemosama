import React from 'react';
import { SafeAreaView, View, Text, TextInput, TouchableOpacity } from 'react-native';
import { style } from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class SignUp extends React.Component {
    render() {
        const { container, backIcon, InputContainer, head, btn, btnTitle } = style;
        return (
            <SafeAreaView style={container}>
                <Icon name="chevron-left" style={backIcon} onPress={() => {
                    if (this.props.navigation.canGoBack()) {
                        this.props.navigation.goBack();
                    }
                }} />

                <Text style={head}>Sign Up</Text>

                <View style={InputContainer}>
                    <TextInput
                        placeholder="E-mail"
                        placeholderTextColor="#aaa"
                    />
                </View>
                <View style={InputContainer}>
                    <TextInput
                        placeholder="Password"
                        placeholderTextColor="#aaa"
                    />
                </View>

                <TouchableOpacity style={btn}>
                    <Text style={btnTitle}>Log in</Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

export default SignUp;