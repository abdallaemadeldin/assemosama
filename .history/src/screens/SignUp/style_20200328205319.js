import { StyleSheet, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignItems: 'center',
        paddingTop: '20%'
    },
    backIcon: {
        fontSize: 35,
        position: 'absolute',
        top: 20,
        left: 20,
        color: "#999"
    },
    InputContainer: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        borderWidth: .5,
        paddingStart: '3%',
        justifyContent: 'center',
        marginVertical: 10
    },
    head: {
        width: '90%',
        fontSize: 30,
        textAlign: 'left',
        marginVertical: '5%',
        color: "#F95866",
        fontWeight: 'bold'
    },
    btn: {
        width: '75%',
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#F95866",
        borderRadius: 22,
        marginVertical: 15
    },
    btnTitle: {
        fontSize: 18,
        color: '#fff'
    }
});