import React from 'react';
import { SafeAreaView, Image, Text, TouchableOpacity } from 'react-native';
import { style } from './style';

class Splash extends React.Component {
    render() {
        const { container } = style;
        return (
            <SafeAreaView style={container}>

            </SafeAreaView>
        );
    }
}

export default Splash;