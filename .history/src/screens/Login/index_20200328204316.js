import React from 'react';
import { SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { style } from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class Login extends React.Component {
    render() {
        const { container, backIcon } = style;
        return (
            <SafeAreaView style={container}>
                <Icon name="chevron-left" style={backIcon} onPress={() => this.props.navigation.goBack()} />
            </SafeAreaView>
        );
    }
}

export default Login;