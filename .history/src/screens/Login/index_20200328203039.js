import React from 'react';
import { SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { style } from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class Login extends React.Component {
    render() {
        const { container } = style;
        return (
            <SafeAreaView style={container}>
                <Icon name="chaveron-left" />
            </SafeAreaView>
        );
    }
}

export default Login;