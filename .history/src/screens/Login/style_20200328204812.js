import { StyleSheet, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const style = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backIcon: {
        fontSize: 35,
        position: 'absolute',
        top: 20,
        left: 20,
        color: "#999"
    },
    InputContainer: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        borderWidth: .5,
        paddingStart: '3%',
        justifyContent: 'center'
    }
});