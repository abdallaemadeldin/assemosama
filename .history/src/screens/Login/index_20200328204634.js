import React from 'react';
import { SafeAreaView, View, Text, TextInput, TouchableOpacity } from 'react-native';
import { style } from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class Login extends React.Component {
    render() {
        const { container, backIcon, InputContainer } = style;
        return (
            <SafeAreaView style={container}>
                <Icon name="chevron-left" style={backIcon} onPress={() => {
                    if (this.props.navigation.canGoBack()) {
                        this.props.navigation.goBack();
                    }
                }} />

                <View style={InputContainer}>

                </View>
            </SafeAreaView>
        );
    }
}

export default Login;